import React from 'react';
import App from '../App';

import Home from '../areas/home/pages';
import Consumidor from '../areas/consumidor/pages';
import Solicitacao from '../areas/solicitacao/pages';



import store from '../redux-config/store';
import {setTitle, setSubtitle, setIcon} from '../layout/actions'


import { Router, IndexRoute, Route, Redirect, browserHistory } from 'react-router'

function handleRouteUpdate() {

    var title = null;
    var subtitle = null;
    var icon = null;

    this.state.routes.forEach(route => {
        title = route.title || title;
        subtitle = route.subtitle || subtitle;
        icon = route.icon || icon;
    });
 

    store.dispatch(setTitle(title));
    store.dispatch(setIcon(icon));
    store.dispatch(setSubtitle(subtitle));
} 
export default (
    <Router history={browserHistory}  onUpdate={handleRouteUpdate}>
        <Route path="/" component={App} >

            <IndexRoute component={Home.IndexPage} title="Página Inicial" subtitle="CDL" icon="home" component={Home.IndexPage}  />  
  
            <Route path="consumidor" title="Consumidor" icon="account-multiple">

                <IndexRoute  subtitle="Busca de Consumidor"  component={Consumidor.IndexPage} />                
                <Route path="details" title="Página Inicial Caraio!"  subtitle="biiiirrrlllll"  component={Consumidor.DetailPage} />                
                <Route path="*" component={Consumidor.IndexPage}/>

            </Route>

            <Route path="solicitacoes" title="Caixa de Entrada" subtitle="Solitiações de Abertura e Baixa" icon="inbox-arrow-down">

                <IndexRoute  subtitle="Busca de Consumidor"  component={Solicitacao.IndexPage} />
                <Route path="details" component={Solicitacao.DetailPage} />                                
                <Route path="nova" component={Solicitacao.NovaPage} />                                
                <Route path="*" component={Solicitacao.IndexPage}/>
                
            </Route> 
        </Route>
    </Router>
); 