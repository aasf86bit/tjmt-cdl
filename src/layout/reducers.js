export function layout(state = {}, {type, title, subtitle, icon, loading, appInfoOpened, appMenuOpened}) {

    switch (type) {
        case 'LAYOUT_SET_TITLE':
            return { ...state, title };

        case 'LAYOUT_SET_SUBTITLE':
            return { ...state, subtitle };

        case 'LAYOUT_SET_ICON':
            return { ...state, icon };

        case 'LAYOUT_SET_LOADING':
            return {...state, loading };

        case 'LAYOUT_TOGGLE_APP_INFO':
            return {...state, appInfoOpened}

        case 'LAYOUT_TOGGLE_APP_MENU':
            return {...state, appMenuOpened}
    }

    return state; 
}