import React from 'react';

import CircularProgress from 'material-ui/CircularProgress';


class LoadingIndicator extends React.Component {

    get style() {
        return {
            container: {
                padding: "32px",
                textAlign: "center"
            },
            indicator: {
                
            },
            title: {
                display: "inline-block",
                fontSize: "32px",
                lineHeight: "32px",
                verticalAlign: "top",
                paddingTop: "32px",
                marginLeft: "16px"
            } 
        };
    }

    render() {
        const {isLoading, loadingText} = this.props;


        if (!isLoading) return null;

        return (
            <div style={this.style.container} >
                <CircularProgress size={1.25} style={this.style.indicator}  />
                <h1 style={this.style.title} >{loadingText || "Carregando..."}</h1>
            </div>
        );

    }

}

export default LoadingIndicator; 