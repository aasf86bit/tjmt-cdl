import {connect} from 'react-redux';
import component from './component';

const mapStateToProps = (state) => ({
    isLoading: state.layout.loading,
    loadingText: state.layout.loadingText
});

const mapDispatchToProps = (dispatch) => ({
    
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(component);