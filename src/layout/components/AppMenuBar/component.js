import React from 'react';

import AppBar from 'material-ui/AppBar';
import SiapLogo from '../SiapLogo';
import FlatButton from 'material-ui/FlatButton';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';
import Drawer from 'material-ui/Drawer';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import Avatar from 'material-ui/Avatar';
import Divider from 'material-ui/Divider';
 
import ArrowDropRight from 'material-ui/svg-icons/navigation-arrow-drop-right';

import { Card, Button, CardHeader, CardFooter, CardBlock,
    CardTitle, CardText } from 'reactstrap';

import {withRouter} from 'react-router';

import {MENU_ITEMS} from 'shared/constants';

const avatarStyle = {
    lineHeight: "24px",
    textAlign: "center",
    backgroundColor: "rgb(255, 136, 14)",
    color: 'white',
    borderRadius: '3px'
}

class AppMenuBar extends React.Component {

    renderItems(items){

        var components = [];

        items.forEach((item, i) => {

            const props = {                
                primaryText: item.title,
                rightIcon: item.items ?<ArrowDropRight /> : null,
                menuItems: item.items ? this.renderItems(item.items) : null,
                leftIcon: <Avatar style={avatarStyle}  size={30} > {item.title.charAt(0)} </Avatar>     
            }

            if(item.action){
                props.onTouchTap = () => this.navTo(item.action);
            }

            components.push( <MenuItem key={i} {...props}    /> );

            if(i < items.length-1) {
                components.push( <Divider /> );                
            }

        }); 

        return components;

    }

    navTo(action) {
        this.props.toggleAppMenu(false);
        this.props.router.push(action);
    }

    render() {

        const {appMenuOpened} = this.props;

        return (
            <Drawer width={300} open={!!appMenuOpened} docked={false}  onRequestChange={() => this.props.toggleAppMenu(false) }>

                <AppBar 
                    onTouchTap={() => this.navTo('/')}
                    style={{borderBottom: '3px solid #ff880e' }}
                    showMenuIconButton={false}
                    titleStyle={{ fontSize: '1.1em', cursor: 'pointer' }}
                    title={<SiapLogo style={{marginTop: '-30px'}}  />} 
                    iconElementRight={
                        <IconButton style={{ marginTop: '-3px' }} onTouchTap={() => this.props.toggleAppMenu(false) }>
                            <FontIcon className='mdi mdi-menu' />
                        </IconButton>
                    }
                    />

                <div className='app-menu'>

                    <div className='scroll-container'>
                       
                        <Menu>
                            {this.renderItems(MENU_ITEMS)}                         
                        </Menu>
                    </div>

                </div>

            </Drawer> 
        )
    }
}

export default withRouter(AppMenuBar);
