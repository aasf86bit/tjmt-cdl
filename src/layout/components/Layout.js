import React from 'react';

import Header from './Header';

import {withRouter} from 'react-router';


class Layout extends React.Component {
 
    render(){

        return (
            <div>
                <Header />
                {this.props.children}
            </div>
        );

    }

}

export default withRouter(Layout);