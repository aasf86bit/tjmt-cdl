import React from 'react';
import './siap-logo.css'; 
import {withRouter} from 'react-router';


class SiapLogo extends React.Component {

    render() {

        return <div className="siap-logo">
            <img src="http://siap2-desenv-a.tjmt.jus.br/assets/logotipos/siap2.svg" />
            <h1> l CDL</h1>
        </div>;

    }

}

export default withRouter(SiapLogo);

