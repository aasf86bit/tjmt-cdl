import React from 'react';

import AppBar from 'material-ui/AppBar'; 
import SiapLogo from '../SiapLogo'; 
import FlatButton from 'material-ui/FlatButton';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';
import Drawer from 'material-ui/Drawer';
import Divider from 'material-ui/Divider';
import Avatar from 'material-ui/Avatar';
import RaisedButton from 'material-ui/RaisedButton';
import AppInfoBar from '../AppInfoBar';
import AppMenuBar from '../AppMenuBar';
import {List, ListItem} from 'material-ui/List';

   
import { Card, Button, CardHeader, CardFooter, CardBlock,
  CardTitle, CardText, Nav, NavItem, NavLink, Popover, PopoverTitle, PopoverContent } from 'reactstrap';

import './style.css';
  
import {withRouter} from 'react-router';

const styleAvatar = {
    width: '30px',
    height: '30px',
    marginLeft: '10px',
    background: '#13496a', 
}

const styleFontIcon = {
    color: '#66b4e3',
    fontSize: '12px',
}

const styleLink = {
    color: '#66b4e3',
    lineHeight: '13px',
    fontSize: '13px',
}

const styleIconSystemVersion = {
    color: '#66b4e3', 
}

const styleNav = {
    float: 'right',
    marginTop: '10px'
}

const styleNavItem = { 
    padding: '0px 15px',
    margin: '0', 
    borderLeft: '1px solid #13496a'
}

class Header extends React.Component {

    navTo(action) {
        this.props.router.push(action);
    }

    get getSystemInfo(){
        
        return (
            <div >
                <Nav inline style={styleNav}>
                    <NavItem style={styleNavItem}>
                        <NavLink href="#" style={styleLink} id='popoverUser'>
                            Olá, Roberto <Avatar style={styleAvatar}> <FontIcon style={styleFontIcon} className='mdi mdi-account'/> </Avatar>
                        </NavLink>
                    </NavItem>
                    <NavItem style={styleNavItem} >
                        <NavLink href="#">
                            <FlatButton label="V.2" className='circle-button-top' labelStyle={styleIconSystemVersion} onTouchTap={() => this.props.toggleAppInfo(true) }/>
                        </NavLink>
                    </NavItem>
                 </Nav>

                <Popover placement="bottom" isOpen={false} target="popoverUser">
                    <PopoverTitle>Seja bem vindo (a),</PopoverTitle>
                    <PopoverContent>
                    
                        <ul className='lista-user-profile'>
                            <li>
                                <span className='mdi mdi-account'></span>
                                Roberto da Cruz Giacomette
                            </li>
                            <li>
                                <span className='mdi mdi-account-card-details'></span>
                                Perfis
                            </li>
                        </ul>

                        <div style={{ borderBottom: 'dotted 1px #ccc'}} /> 

                        <div>                            
                        
                        </div>

                    </PopoverContent>
                </Popover>
            </div> 
        )
    }

    render() {

        const {appInfoOpened} = this.props; 

        return (
            <div>

                <AppBar 
                    
                    onTitleTouchTap={() => this.navTo('/')}
                    title={<SiapLogo />} 
                    titleStyle={{ cursor: 'pointer' }}
                    style={{ borderBottom: '3px solid #ffa500' }}
                    onLeftIconButtonTouchTap={() => this.props.toggleAppMenu(true)}
                    iconElementRight={this.getSystemInfo} />

                    <AppInfoBar />

                    <AppMenuBar />
            </div>
        )
    }
}

export default withRouter(Header);
