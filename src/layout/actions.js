export function setTitle(title){
    return {
        type: "LAYOUT_SET_TITLE",
        title 
    };
}

export function setSubtitle(subtitle){
    return {
        type: "LAYOUT_SET_SUBTITLE", 
        subtitle 
    };
}

export function setIcon(icon){
    return {
        type: "LAYOUT_SET_ICON", 
        icon
    };
}

export function toggleAppInfo(appInfoOpened){
    return {
        type: "LAYOUT_TOGGLE_APP_INFO", 
        appInfoOpened
    };
}

export function toggleAppMenu(appMenuOpened){
    return {
        type: "LAYOUT_TOGGLE_APP_MENU", 
        appMenuOpened
    };
}


export function setLoading(loading){
    return {
        type: "LAYOUT_SET_LOADING",
        loading
    };
}