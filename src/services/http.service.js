import {BASE_URL} from 'shared/constants';

export default class HttpService {

    request(uri,  method,body, headers){

        if(body){
            headers = headers || {};
            headers["Content-Type"] = "application/json";
            body = JSON.stringify(body);
        }

        return fetch(`${BASE_URL}${uri}`, { method, headers, body }).then( x => x.json());
    }
 

    get(uri){
        return this.request(uri, "GET");
    }

    post(uri, body){
        return this.request(uri, "POST", body);
    }

    put(uri){
        return this.request(uri, "PUT");
    }

    delete(uri){
        return this.request(uri, "DELETE");
    }

}