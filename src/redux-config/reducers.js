import {layout} from "../layout/reducers";
import {consumidor} from "areas/consumidor/reducers";
import * as solicitacao from "areas/solicitacao/reducers";

import {combineReducers} from 'redux';

export default combineReducers({
    ...solicitacao,
    consumidor,
    layout
})