import HttpService from '../services/http.service';
import faker from 'faker';

faker.locale = "pt_BR";
var idG = 1;

function createPerson(){
    return {
        id: ++idG,
        nome: faker.name.findName(),
        cpf: faker.helpers.replaceSymbolWithNumber("###.###.###-##"),
        sexo: faker.random.number(1) ? 'm' : 'f',
        dataNascimento: faker.date.past(18, new Date(1993, 1,1)),
        nomePai: faker.name.findName(null, null, 0),
        nomeMae: faker.name.findName(null, null, 1),
        telefone: faker.phone.phoneNumber(),
        rg: faker.random.number({ min: 10000, max: 999999 }) + "-" + faker.random.number({ min: 1, max: 1 }),
    }  
}

function createPeople(){
    var p =[];
    var l = Math.round((Math.random() * 5)  + 1);
    for(var i = 0; i < l; i++) p.push(createPerson());
    return p;
}


export default class ConsumidorApi {

    constructor(){
        this.http = new HttpService();
    }

    getConsumidores(search){
        //return this.http.get(`/consumidores?query=${search}`);

        return new Promise(function(resolve, reject){

            setTimeout(() => resolve(createPeople()) , Math.round(Math.random() * 1000) + 200);

        });

    }

}