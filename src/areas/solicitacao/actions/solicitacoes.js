import {setLoading} from 'layout/actions';
import SolicitacaoApi from 'api/solicitacao.api';


const api = new SolicitacaoApi();

export function requestSolicitacoes(search) {
    return {
        type: "FETCH_SOLICITACAO_REQUEST",
        search,
        grupos: []
    };
}

export function receiveSolicitacoes(error, grupos) {
    var selecionado = null;
    if (grupos && grupos.length == 1) {
        selecionado = grupos[0].id;
    }

    return {
        type: "FETCH_SOLICITACAO_" + (error ? "FAILURE" : "SUCCESS"),
        error,
        grupos,
        selecionado
    };
}



export function fetchSolicitacoes(search) {

    return function (dispatch) {

        dispatch(setLoading(true))
        dispatch(requestSolicitacoes(search));

        api.getSolicitacoes(search)
            .then(grupos => {
                dispatch(receiveSolicitacoes(null, grupos))
                dispatch(setLoading(false))
            })
            .catch(err => {
                dispatch(receiveSolicitacoes(err, null))
                dispatch(setLoading(false))
            });

    };

}