import IndexPage from './IndexPage';
import DetailPage from './DetailPage';
import NovaPage from './NovaSolicitacaoPage';

export default { IndexPage, DetailPage, NovaPage };