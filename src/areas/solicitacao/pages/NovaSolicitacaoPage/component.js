import React from 'react';
import style from './style.usable.css';

import {withRouter} from 'react-router';

import {Input, Label, Row, Col} from 'reactstrap';
import RaisedButton from 'material-ui/RaisedButton';
import FontIcon from 'material-ui/FontIcon';
import Divider from 'material-ui/Divider';
import MenuItem from 'material-ui/MenuItem';
import SelectField from 'material-ui/SelectField';

import ColCard from 'layout/components/ColCard';


import InputBox from '../../components/ProcessoInputBox';
import LoadingIndicator from 'layout/components/LoadingIndicator';

import ListaPartes from '../../components/ListaPartes';
import ModalParte from '../../components/ModalParte';
import FormularioSolicitacao from '../../components/FormularioSolicitacao';

import linkable from 'shared/components/linkable';

class NovaSolicitacaoPage extends React.Component {

    constructor(props) {
        super(props);
        this.link = linkable.bind(this);

        this.state = {
            numeroUnico: ""
        };

    }

    componentWillMount() {
        style.use();
    }

    componentWillUnmount() {
        style.unuse();
    }



    render() {

        const {numeroUnico} = this.state;
        const {isLoading, loadProcesso} = this.props;

        return (
            <div>
                <ModalParte />
                <Row>
                    <ColCard  >
                        <InputBox />
                    </ColCard>
                </Row>
                <Row>
                    <ListaPartes />
                </Row>
                <Row style={{ paddingBottom: "64px" }} >
                    <FormularioSolicitacao />
                </Row>
            </div>
        );

    }

}



export default withRouter(NovaSolicitacaoPage);