import React from 'react';

import {withRouter} from 'react-router';

import {Input} from 'reactstrap';
import RaisedButton from 'material-ui/RaisedButton';
import FontIcon from 'material-ui/FontIcon';
import Card from 'material-ui/Card';

import ListaSolicitacoes from '../../components/ListaSolicitacoes'; 
import LoadingIndicator from 'layout/components/LoadingIndicator';
import withCardPage from 'layout/components/CardPage';

class IndexPage extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            search: ""
        };
        
    }

    componentWillMount(){
        this.props.loadSolicitacoes();
    }

    
    link(prop){
        return {
            value: this.state[prop],
            onChange: (e) => this.setState({ [prop]: e.target.value })
        }
    }

    navToNovaSolicitacao(){
        this.props.router.push("solicitacoes/nova");
    }

    render(){
 

        return (
            <div>
                    <div style={{ marginTop: "8px", marginLeft:  "24px" }}>
                        <RaisedButton primary onClick={() => this.navToNovaSolicitacao()} icon={<FontIcon className="mdi mdi-plus" />} label="Nova Solicitação"  />
                    </div>
                    <LoadingIndicator />
                    <ListaSolicitacoes />
            </div>
                    
        );

    }

}



export default withCardPage(withRouter(IndexPage));