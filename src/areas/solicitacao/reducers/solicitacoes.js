export function solicitacoes(state = {}, {type, error, grupos}){

    switch(type){

        case "FETCH_SOLICITACAO_REQUEST":
            return { ...state, fetching: true, grupos };
        case "FETCH_SOLICITACAO_SUCCESS":
            return { ...state, fetching: false, error: null, grupos };        
        case "FETCH_SOLICITACAO_FAILURE":
            return { ...state, fetching: false, error, grupos: []  };        

    }

    return state;

}
