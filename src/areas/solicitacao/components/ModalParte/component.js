import React from 'react';
import moment from 'moment';
import {withRouter} from 'react-router';

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';

import InfoTable from './info-table';

class ModalParte extends React.Component {



    render() {

        const {parteSelecionada} = this.props;

        if (!parteSelecionada) return null;

        const actions = [
            <FlatButton
                label="Fechar"
                primary
                keyboardFocused
                onTouchTap={() => this.props.close() }
                />,
        ];

        return (
            <Dialog
                title={parteSelecionada.nome}
                actions={actions}
                modal={false}
                open
                onRequestClose={() => this.props.close() }
                >
                {parteSelecionada.invalida ?
                    <p className="text-danger">
                        <i className="mdi mdi-alert" style={{ marginRight: "8px" }} />
                        Os dados que estão incompletos devem ser corrigidos no <strong>sistema de Origem</strong>.
                    </p>
                    : null}
                <InfoTable parte={parteSelecionada} />
            </Dialog>
        );
    }

}

export default withRouter(ModalParte);