import React from 'react';
import moment from 'moment';
import style from './style.usable.css';
import {withRouter} from 'react-router';

import {List, ListItem} from 'material-ui/List';
import Divider from 'material-ui/Divider';
import Subheader from 'material-ui/Subheader';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';
import Checkbox from 'material-ui/Checkbox';
import {Col} from 'reactstrap';
import ColCard from 'layout/components/ColCard';


class ColumnPartes extends React.Component {



    showDetailsIcon(parte) {
        var flavor = {
            className: "mdi mdi-magnify"
        };

        if (parte.invalida) {
            // flavor.className = "mdi mdi-alert"
            flavor.color = "#ff0000";
        }

        return (
            <IconButton   >
                <FontIcon {...flavor} onClick={() => this.props.showDetalhesParte(parte) } />
            </IconButton>
        );
    }

    checkbox(parte) {

        const {selecionados} = this.props;
        const isSelecionado = selecionados.some(x => x.id == parte.id);

        return <Checkbox checked={isSelecionado} onCheck={(ev, checked) => this.props.selecionarParte(parte, checked) } />

    }

    render() {

        const {partes, title} = this.props;

        return (
            <ColCard lg="6" >
                <h1><strong>Parte {title}</strong></h1>
                <List>
                    <Divider />

                    {
                        partes.map(parte => <ListItem
                            leftCheckbox={this.checkbox(parte) }
                            key={parte.id}
                            primaryText={parte.nome}
                            style={{ color: parte.invalida && "red"  }}
                            rightIconButton={this.showDetailsIcon(parte) }
                            secondaryText={parte.cpf || "CPF não cadastrado."} />
                        )
                    }


                </List>
            </ColCard>

        );
    }

}

export default withRouter(ColumnPartes);