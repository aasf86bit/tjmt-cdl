import {fetchProcesso, clearProcesso} from '../../actions'; 
import {connect} from 'react-redux';
import component from './component';

const mapStateToProps = (state) => ({ 
    isLoading: state.solicitacao.fetching,
    numeroUnico: state.solicitacao.processo && state.solicitacao.processo.numeroUnico,
    errorMessage: state.solicitacao.error 
});

const mapDispatchToProps = (dispatch) => ({
    loadProcesso(numeroUnico){
        dispatch(fetchProcesso(numeroUnico))
    },
    clearProcesso(){
        dispatch(clearProcesso())
    }
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(component);


