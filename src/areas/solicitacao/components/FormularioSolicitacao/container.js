import {selecionarParte, showDetalhesParte, setMotivo, setTipo} from "../../actions"; 
import {connect} from 'react-redux';
import component from './component';

const mapStateToProps = (state) => ({ 
    motivo: state.solicitacao.motivo,
  
    
    tipo: state.solicitacao.tipo,
      

    processo: state.solicitacao.processo,
    autoresSelecionados: state.solicitacao.autoresSelecionados || [],
    reusSelecionados: state.solicitacao.reusSelecionados || [],
    partesInvalidas: state.solicitacao.partesInvalidas || []
});

const mapDispatchToProps = (dispatch) => ({
    selecionarParte(parte, selecionado){
        dispatch(selecionarParte(parte, selecionado));
    },
    showDetalhesParte(parte){
        dispatch(showDetalhesParte(parte));
    },
    setMotivo(motivo){
        dispatch(setMotivo(motivo));
    },
    setTipo(tipo){
        dispatch(setTipo(tipo));
    },
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(component);


