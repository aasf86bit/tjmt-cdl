import React from 'react';
import moment from 'moment';
import {withRouter} from 'react-router';
import style from './style.usable.css';
import {List, ListItem} from 'material-ui/List';
import Divider from 'material-ui/Divider';
import Subheader from 'material-ui/Subheader';
import FontIcon from 'material-ui/FontIcon';

import Items from './items'; 

import {Card, CardBlock, CardTitle, CardSubtitle} from 'reactstrap';


class ListaSolicitacoes extends React.Component {

    componentWillMount() {
        style.use(); 

    }

    componentWillUmount() {
        style.unuse();
    }

    navTo() {
        this.props.router.push("solicitacoes/details");
    }

    render() {

        var {grupos} = this.props;
        

        return (
            <div className="list-bordered">

                <List>

                    {grupos.map(({title, icon, items}) => {

                        return (
                            <div>
                                <Subheader className="header">
                                    <i className={"mdi mdi-" + icon} />
                                    <span style={{ marginLeft: "8px" }} >{title}</span>
                                </Subheader>

                                <Items items={items} title={title} />

                            </div>
                        );

                    }) }



                </List>
            </div>
        );
    }

}

export default withRouter(ListaSolicitacoes);