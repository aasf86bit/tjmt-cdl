import React from 'react';
import {Row, Col, Label} from 'reactstrap';
import Chip from 'material-ui/Chip'; 

const styles = {
    chip: {
        margin: 8,
    },
    redChips: {
        margin: 8,
        border: "1px dotted red",
        color: "red"
    },
    wrapper: {
        display: 'flex',
        flexWrap: 'wrap',
    },
};


class ChipsPartes extends React.Component {

    renderChips(partes) {
        const {partesInvalidas} = this.props;
        return partes.map(parte => {

            var invalid = partesInvalidas.some(inv => inv.id == parte.id);
            var flavor = {
                style: invalid ? styles.redChips : styles.chips
            };
            if (invalid) flavor.labelColor = "red";

            return (
                <Chip {...flavor} key={parte.id} onClick={() => this.props.showDetalhesParte(parte) }  onRequestDelete={() => this.props.selecionarParte(parte, false) }>
                    {parte.nome}
                </Chip>
            );

        });
    }

    render() {

        const {partes, tipoTexto} = this.props;

         return (
            <Row>
                <Col md="8" lg="6" style={{ marginTop: "18px" }} >

                    <Label className="text-info">Será solicitado para as seguintes <strong>Parte(s) {tipoTexto}(s): </strong></Label><br/>
                    <div style={styles.wrapper}>
                        {partes.length ?
                            this.renderChips(partes) :
                            <em className="text-muted">Selecione na caixa acima a(s) parte(s) {tipoTexto.toLowerCase() }(s) </em>
                        }
                    </div>

                </Col>
            </Row>
        );
    }

}

export default ChipsPartes;