import {selecionarParte, showDetalhesParte} from "../../actions";
import {connect} from 'react-redux';
import component from './component';

const mapStateToProps = (state) => ({
    partesInvalidas: state.solicitacao.partesInvalidas || []

});

const mapDispatchToProps = (dispatch) => ({
    selecionarParte(parte, selecionado) {
        dispatch(selecionarParte(parte, selecionado));
    },
    showDetalhesParte(parte) {
        dispatch(showDetalhesParte(parte));
    }
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(component);


