import React from 'react';
import moment from 'moment';
 


class InforTable extends React.Component {

    get containerStyle() {

        return {
            padding: 18
        };

    }

    render() {

        const {consumidor} = this.props;

        if (!consumidor) return null;

        return (
            <table>

                <tr>
                    <th>CPF</th>
                    <td className="info">{this.props.consumidor.cpf}</td>
                </tr>
                <tr>
                    <th>RG</th>
                    <td className="info">{this.props.consumidor.rg}</td>
                </tr>
                <tr>
                    <th>Sexo</th>
                    <td className="info">{this.props.consumidor.sexo == 'm' ? "Masculino" : "Feminino"}</td>
                </tr>
                <tr>
                    <th>Telefone</th>
                    <td className="info">{this.props.consumidor.telefone}</td>
                </tr>
                <tr>
                    <th>Data de Nascimento</th>
                    <td className="info">{moment(this.props.consumidor.dataNascimento).format("DD/MM/YYYY") }</td>
                </tr>
                <tr>
                    <th>Filiação</th>
                    <td className="info">
                        {this.props.consumidor.nomePai} 
                        <br/>
                        {this.props.consumidor.nomeMae}
                    </td>
                </tr>
            </table>

        );

    }

}

export default InforTable;

