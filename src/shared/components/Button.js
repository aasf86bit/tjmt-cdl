import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';


export default (props) => {

    var {style, buttonStyle} = props;

    style = style || {};
    buttonStyle = buttonStyle || {};

    var newStyle = {...style, borderRadius: 0};

    var newButtonStyle = {...buttonStyle, borderRadius: 0};

    console.log(newStyle, newButtonStyle);

    return <RaisedButton {...props} style={newStyle} buttonStyle={newButtonStyle} />

};