import React from 'react';

export default function link(prop) {

    var state = this.state;
    var setState = this.setState.bind(this);

    if (typeof prop == "string") {
        prop = [prop];
    }

    return {
        value: state[prop],
        onChange: (e, i, v) => {
            var value = v || e.target.value;

            setState({
                [prop]: value
            });
        }

    };

} 